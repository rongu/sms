<?php

/*
 * You can place your custom package configuration in here.
 */

return [

    'default_provider' => env('SMS_DEFAULT_PROVIDER', 'dhiraagu'),

    'dhiraagu' => [

        'user_id' => env('SMS_DHIRAAGU_USER_ID'),
        'password' => env('SMS_DHIRAAGU_PASSWORD'),
        'send_get_url' => env('SMS_DHIRAAGU_SEND_GET_URL'),
        'send_xml_post_url' => env('SMS_DHIRAAGU_SEND_XML_POST_URL'),
    
        'log_model' => \Rongu\Sms\Models\DhiSmsLog::class,
    ],

    'ooredoo' => [
        'base_url' => env('SMS_OOREDOO_BASE_URL'),
        'token' => env('SMS_OOREDOO_TOKEN'),
        'username' => env('SMS_OOREDOO_USERNAME'),
        'access_key' => env('SMS_OOREDOO_ACCESS_KEY'),
    
        'log_model' => \Rongu\Sms\Models\OoredooSmsLog::class,
    ],
    
    
    'single_sms_max_length' => env('SINGLE_SMS_MAX_LENGTH', 160),
    'multi_sms_max_length' => env('MULTI_SMS_MAX_LENGTH', 153),
    'multi_sms_max_messages' => env('MULTI_SMS_MAX_MESSAGES', 3),

];